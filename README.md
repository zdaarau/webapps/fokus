# FOKUS

This repository contains code and configuration related to the FOKUS post-voting surveys by the [Centre for Democracy Studies Aarau
(ZDA)](https://www.zdaarau.ch/) at the [University of Zurich](https://www.uzh.ch/), Switzerland.

-   [**`fly_rstudio-server/`**](fly_rstudio-server) contains the configuration for our preconfigured [RStudio
    Server](https://posit.co/products/open-source/rstudio-server/), also hosted on Fly, which allows for hassle-free editing of the [FOKUS
    reports](https://gitlab.com/zdaarau/fokus_reports).

The [DNS](https://en.wikipedia.org/wiki/Domain_Name_System) entries for the `fokus.ag` domain involved in the above setup are recorded in [`dns_records.toml` in
the `zdaarau/websites/fokus.ag` repository](https://gitlab.com/zdaarau/websites/fokus.ag/-/blob/master/dns_records.toml).

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
