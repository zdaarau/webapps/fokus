#!/bin/bash
#
# Deploy new RStudio Server release on Fly
#
# Requirements:
#
# - CLI tools:
#   - [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) (obviously)
#   - [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
#   - [`dasel`](https://daseldocs.tomwright.me/)
#   - [`flyctl`](https://fly.io/docs/flyctl/)
#   - [`sd`](https://github.com/chmln/sd)

# exit immediately if a cmd fails
set -e

# set target Fly app name
FLY_APP_NAME='fokus-rstudio'

# basic assertions
## ensure all required CLI tools are available
if ! command -v dasel >/dev/null ; then
  echo "\`dasel\` is required but not found on PATH. See https://daseldocs.tomwright.me/installation"
  exit 1
fi
if ! command -v flyctl >/dev/null ; then
  echo "\`flyctl\` is required but not found on PATH. See https://fly.io/docs/flyctl/"
  exit 1
fi
if ! command -v sd >/dev/null ; then
  echo "\`sd\` is required but not found on PATH. See https://github.com/chmln/sd#readme"
  exit 1
fi
## ensure flyctl is authorized to manage the app
if ! flyctl apps list | grep -q "\b${FLY_APP_NAME}\b"; then
  echo "The currently active Fly account is not authorized to manage the \`${FLY_APP_NAME}\` Fly app. Do you need to \`flyctl auth login\` into another account?"
  exit 1
fi

# define convenience functions
get_fly_status() {
  FLY_CONFIG="$(flyctl status --json)"
  FLY_MACHINE_ID="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].id' | sd --fixed-strings '"' '')"
  FLY_MACHINE_STATE="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].state' | sd --fixed-strings '"' '')"
  FLY_MACHINE_CHECK_STATUS="$(echo "${FLY_CONFIG}" | dasel --read json --selector 'Machines.[0].checks.[0].status' | sd --fixed-strings '"' '')"
}

strip_quote_wrapping() {
  local _result="$1"

  if echo "${_result}" | grep -q "^'.*'$" ; then
    _result=$(echo "${_result}" | grep -Po "(?<=^').*(?='$)")
  elif echo "${_result}" | grep -q '^".*"$' ; then
    _result=$(echo "${_result}" | grep -Po '(?<=^").*(?="$)')
  fi
  
  echo "${_result}"
}

# ask whether to proceed if Fly app is not stopped
get_fly_status

if [ "${FLY_MACHINE_STATE}" != "stopped" ] ; then
  read -r -p "The \`${FLY_APP_NAME}\` Fly machine is currently \"${FLY_MACHINE_STATE}\". Do you still wanna proceed with deployment and replace the running instance (y/n)? " answer
  case ${answer:0:1} in
        [Yy]* ) ;;
        * ) exit 1;;
    esac
fi

# read in secrets
if [ -f .secrets ] ; then
  PASSWORD=$(strip_quote_wrapping "$(grep -Po '(?<=PASSWORD=)\S+' .secrets)")
  BATCH_USER_CREATION=$(strip_quote_wrapping "$(grep -Po '(?<=BATCH_USER_CREATION=)\S+' .secrets)")
  if [ -z "${PASSWORD}" ] ; then
    echo "Required \`PASSWORD\` is not set in local \`.secrets\` file."
    exit 1
  fi
  if [ -z "${BATCH_USER_CREATION}" ] ; then
    echo "Required \`BATCH_USER_CREATION\` is not set in local \`.secrets\` file."
    exit 1
  fi
else
  echo "Local \`.secrets\` file is missing, which must contain \`PASSWORD\` and \`BATCH_USER_CREATION\`, see the \`README.md\`."
  exit 1
fi

# build and deploy image
# NOTE: without the `--no-cache` flag, i.a. user creation will be cached
flyctl deploy --build-secret PASSWORD="${PASSWORD}" \
              --build-secret BATCH_USER_CREATION="${BATCH_USER_CREATION}" \
              --ha=false \
              --local-only \
              --no-cache

# start the Fly machine after it has been replaced
get_fly_status
while [ "${FLY_MACHINE_STATE}" == 'replacing' ] ; do
  sleep 1
  get_fly_status
done
flyctl machine start "${FLY_MACHINE_ID}"
get_fly_status
while [ "${FLY_MACHINE_STATE}" == 'started' ] && [ "${FLY_MACHINE_CHECK_STATUS}" == 'warning' ] ; do
  sleep 1
  get_fly_status
done

# initialize persistent storage
./run_update_persistent.sh
